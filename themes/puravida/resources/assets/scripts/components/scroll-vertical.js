export default {
  init() {
    // JavaScript to be fired on all pages

    // this.mouse('.box-scroll');
    this.touch('.box-scroll');
    // this.mouse('.box-scroll-author');
    this.touch('.box-scroll-author');

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },

  mouse(elm) {
    const mouseWheel = document.querySelector(elm);
    if (!mouseWheel) { return; }
    mouseWheel.addEventListener('wheel', function (e) {
      const race = 15; // How many pixels to scroll

      if (e.deltaY > 0) // Scroll right
        mouseWheel.scrollLeft += race;
      else // Scroll left
        mouseWheel.scrollLeft -= race;
      e.preventDefault();
    });
  },

  touch(elm) {

    const slider = document.querySelector(elm);
    if (!slider) { return; }
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener('mousedown', (e) => {
      isDown = true;
      slider.classList.add('active');
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
      console.log(walk);
    });
  },
};
