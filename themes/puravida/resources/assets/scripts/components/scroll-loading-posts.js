import $ from 'jquery';

export default {
  init() {
    // JavaScript to be fired on the home page
    var currentscrollHeight = 0;
    let paged = 1;
    let callReturn = false;
    const max_num_pages = $('.main2').attr('data-max_num_pages');
    $(window).on('scroll', function () {
      //const pagination = $(document).find('.navigation.pagination');
      //const lastPage = pagination.find('.next');
      const scrollHeight = $(document).height();
      const scrollPos = Math.floor($(window).height() + $(window).scrollTop());
      const isBottom = scrollHeight - 800 < scrollPos;
      if (paged < max_num_pages && isBottom && currentscrollHeight < scrollHeight && callReturn === false) {
        //paged = pagination.find('.nav-links .current').next().text() ?? null;
        //pagination.remove();
        paged = paged + 1;
        callData();
        currentscrollHeight = scrollHeight;
      }

    });

    function callData() {
      callReturn = true;
      // console.log('Carregando...', paged, window.puravida_ajax_object);
      $('.main2').append($('<div class="loading-post" />').text('Carregando mais posts...'));
      jQuery.get(
        window.puravida_ajax_object.ajax_url,
        {
          action: 'get_modal',
          security: window.puravida_ajax_object.security,
          paged: paged,
          author: window.puravida_ajax_object.author,
          category: window.puravida_ajax_object.category,
        }).done(function (data) {
          //console.log(data);
          $('.main2').append(data);
          $(document).find('.main2 .loading-post').remove();
          callReturn = false;
        });

    }
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};


