import 'jquery';

export default {
  init() {
    // JavaScript to be fired on the home page

    let fixme = $('.fixme');
    const header = $('header');
    const win = $(window);
    var fixmeTop = fixme.offset().top;

    function fnFixmeTop() {
      var currentScroll = $(window).scrollTop();
      if (currentScroll >= (fixmeTop + 100)) {
        fixme.addClass('is-ps-fixed').css({
          top: header.css('height'),
        });
      } else {
        fixme.removeClass('is-ps-fixed');
      }
    }
    win.scroll(fnFixmeTop);
    fnFixmeTop();

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};


