import 'jquery';

export default {
  init() {
    // JavaScript to be fired on the home page
    let processScroll = () => {
      let docElem = document.documentElement,
        docBody = document.body,
        scrollTop = docElem['scrollTop'] || docBody['scrollTop'],
        scrollBottom = (docElem['scrollHeight'] || docBody['scrollHeight']) - window.innerHeight - (document.getElementById('comments').offsetHeight + document.getElementById('footer').offsetHeight + 50),
        scrollPercent = (scrollTop / scrollBottom) * 100 + '%';
      document.getElementById('progress-bar').style.setProperty('--scrollAmount', scrollPercent);
    }

    document.addEventListener('scroll', processScroll);
    processScroll();

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};


