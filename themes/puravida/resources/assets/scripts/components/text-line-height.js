import $ from 'jquery';

export default {
  init() {
    // JavaScript to be fired on the home page

    $(document).on('click', '.flx.fl-2:not(.active)', function () {
      const _this = $(this);
      _this.addClass('active');
      $('.flx.fl-1').removeClass('active');
      $('.inner-page').removeClass('is-fln-1').addClass('is-fln-2');
    });
    $(document).on('click', '.flx.fl-1:not(.active)', function () {
      const _this = $(this);
      _this.addClass('active');
      $('.flx.fl-2').removeClass('active');
      $('.inner-page ').removeClass('is-fln-2').addClass('is-fln-1');
    });
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
