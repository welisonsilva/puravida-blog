import $ from 'jquery';

export default {
  init() {
    // JavaScript to be fired on the home page

    $(document).on('click', '.fzx.fl-2:not(.active)', function () {
      const _this = $(this);
      _this.addClass('active');
      $('.fzx.fl-1').removeClass('active');
      $('.inner-page').removeClass('is-fzn-1').addClass('is-fzn-2');
    });
    $(document).on('click', '.fzx.fl-1:not(.active)', function () {
      const _this = $(this);
      _this.addClass('active');
      $('.fzx.fl-2').removeClass('active');
      $('.inner-page ').removeClass('is-fzn-2').addClass('is-fzn-1');
    });
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
