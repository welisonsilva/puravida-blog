import $ from 'jquery';

export default {
  init() {
    const slider = $('.slick');

    function slickAction(event, slick) {
      let currentSlidesToShow = slick.activeBreakpoint ? slick.breakpointSettings[slick.activeBreakpoint].slidesToShow : slick.slickGetOption('slidesToShow');
      //currentSlide = slick.slickGetOption('slidesToShow');
      //img = currentSlide == (slick.slideCount-(_slidesToShow-1)) ? slick.slideCount : currentSlide+(_slidesToShow-1);
      //console.log('img',currentSlide, img);
      $('.slick').removeClass('loading');
      $('.slick-next.slick-arrow').css({ 'background-image': 'url(' + slider.find('.item').eq(slick.currentSlide + (currentSlidesToShow * 2)).find('img.img-responsive.desktop').attr('data-slidecropimg') + ')' });
      $('.slick-prev.slick-arrow').css({ 'background-image': 'url(' + slider.find('.item').eq(slick.currentSlide - (currentSlidesToShow - (slick.slideCount - 1))).find('img.img-responsive.desktop').attr('data-slidecropimg') + ')' });
      console.log(slick.slideCount, slick.currentSlide, currentSlidesToShow, slick.slickGetOption('slidesToShow'), event, slick);
    }
    slider.on('init', slickAction);
    slider.on('reInit', slickAction);
    slider.slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      dots: false,
      arrows: true,
      prevArrow: '<button type="button" class="slick-prev slick-arrow"><svg class="is-no-fill slick-prev"><use xlink:href="/wp-content/themes/puravida/resources/assets/images/common.svg#prev-arrow" /></svg></button>',
      nextArrow: '<button type="button" class="slick-next slick-arrow"><svg class="is-no-fill slick-next"><use xlink:href="/wp-content/themes/puravida/resources/assets/images/common.svg#next-arrow" /></svg></button>',
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            //arrows: false,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            // centerMode: true,
            // centerPadding: '10px',
            //arrows: false,
          },
        },
      ],
    });


    // slider.on('swipe', function (event, slick, direction) {
    //   console.log('swipe');
    // });




    slider.on('afterChange', slickAction);

    // slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    //   console.log('beforeChange::nextSlide', nextSlide);
    // });
  },
  finalize() {

  },
};
