import $ from 'jquery';

export default {
  init() {
    // JavaScript to be fired on the home page

    $(function ($) {
      var $el = $('.post__image');
      $(window).on('scroll', function () {
        var scroll = $(document).scrollTop();
        $el.css({
          'background-position': '50% ' + (-.4 * scroll) + 'px',
        });
      });
    });
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};


