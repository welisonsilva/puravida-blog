import 'jquery';
import toShare from '../components/toshare.js';
import ReadingProgressIndicator from '../components/reading-progress-indicator.js';
import ParalaxScroll from '../components/paralax-scroll.js';
import TextLineHeight from '../components/text-line-height.js';
import TextFontSize from '../components/text-font-size.js';

export default {
  init() {
    toShare.init();
    ReadingProgressIndicator.init();
    ParalaxScroll.init();
    TextLineHeight.init();
    TextFontSize.init();
    // JavaScript to be fired on the home page
    // jQuery('.alignfull').before('</article></div></section>');
    // jQuery('.alignfull').after('<section class="l-common-wrapper"><div class="l-common-row"><article class="inner-page is-w-75">');
  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};


