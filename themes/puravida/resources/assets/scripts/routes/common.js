import $ from 'jquery';
import scrollVertical from '../components/scroll-vertical.js';
import sliderDestaques from '../components/slider-destaques.js';
import ScrollLoadingPosts from '../components/scroll-loading-posts.js';

export default {
  init() {
    // JavaScript to be fired on all pages
    sliderDestaques.init();

    $(document).on('click', '.nav-open', function () {
      $('.menu-menu-principal-container').toggleClass('active');
    });

  },
  finalize() {
    scrollVertical.init();
    ScrollLoadingPosts.init();
  },
};
