<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);

/**
* INICIA CARBON FIELDS
*/

function crb_load()
{
    \Carbon_Fields\Carbon_Fields::boot();
}
add_action('after_setup_theme', 'crb_load');

/**
 * Adicionar icones ao menu baseado na class name ico-*
 *
 * @param mixed $items
 * @param mixed $args
 * @return void
 */
function main_menu_filter($items, $args)
{
    foreach ($items as $item) {
        $classString = join($item->classes, '|');
        if (preg_match('/\ico-\b/', $classString)) {
            $pos = strpos($classString, 'ico');
            $pos1 = strpos($classString, '|');
            $svgName = str_replace('ico-', '', substr($classString, $pos, $pos1));
            $svg = '<svg class="is-no-fill '.$svgName.'"><use xlink:href="'.App::getPatchImageCommonSVG().'#'.$svgName.'" /></svg>';
            $item->title =  $svg.'<span>'.$item->title.'</span>';
        }
    }
    return $items;
}

add_filter('wp_nav_menu_objects', 'main_menu_filter', 10, 4);


function bts_add_html_to_content($content)
{
    // $html_segment = '<p>Text to be added.</p>';
    // $content = $html_segment . $content . $html_segment;

    // $content = preg_replace("/<figure.+?id=\"(.+?)\".+?aria-describedby=\"(.+?)\".+?style=\"(.+?)\".+?class=\"(.+?)\"/Si", '</article></div></section><figure id="\1" aria-describedby="\2" style="\3" class="\4"', $content);
    // $content = preg_replace("/<\/figure>/Si", '</figure><section class="l-common-wrapper"><div class="l-common-row"><article class="inner-page is-w-75">', $content);

    // $content = preg_replace("/<figure.+?id=\"(.+?)\".+?aria-describedby=\"(.+?)\".+?style=\"(.+?)\".+?class=\"(.+?)\"/Si", '<div class="figura \4"><figure id="\1" aria-describedby="\2" style="\3" class="\4"', $content);
    // $content = preg_replace("/<div class=\"figura wp-caption alignfull\"/Si", '<div class="figura wp-caption alignfull dois"', $content);
    // $content = preg_replace("/<\/figure>/Si", '</figure></div>', $content);
    preg_match('#<figure[^>]*alignfull[^>]*>.*?</figure>#is', $content, $matches);
    $content = preg_replace('#<figure[^>]*alignfull[^>]*>.*?</figure>#is', '</div></article></section>'.$matches[0].'<section class="l-common-wrapper"><div class="l-common-row"><div class="sidebar d-flex is-justify-start"><div class="toshare"></div></div><article class="inner-page is-w-75 is-ws-100 is-fln-1 is-fzn-1">', $content);
    return $content;
}
add_filter('the_content', 'bts_add_html_to_content', 99);




function child_comment_counter($id)
{
    global $wpdb;
    $query = "SELECT COUNT(comment_post_id) AS count FROM `".$wpdb->prefix."comments` WHERE `comment_approved` = 1 AND `comment_parent` = ".$id;
    $children = $wpdb->get_row($query);
    $total = $children->count;
    return $total > 0 ? '<svg class="is-no-fill replay"><use xlink:href="'.App::getPatchImageCommonSVG().'#comment-replay"></use></svg>' . $total . " <span>resposta(s)</span>" : "";
}
function fnComments($comment, $args, $depth)
{
    if ('div' === $args['style']) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    //var_dump($comment);?>
    <<?php echo $tag ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">

    <?php if ('div' != $args['style']) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>

    <div class="comment__text">
        <?php comment_text(); ?>
        <?php if ($comment->comment_approved == '0') : ?>
            <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.'); ?></em>
            <br />
        <?php endif; ?>
    </div>
    <div class="comment__info --vcard d-flex">
        <div class="comment__photo"><?php echo get_avatar($comment, $args['avatar_size']); ?></div>
        <div class="comment__name"> <?php printf(__('<span class="fn">%s</span>'), get_comment_author()); ?></div>
        <div class="comment__hrs"> <?php
            //printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time());
            $start_date = new DateTime($comment->comment_date);
    $since_start = $start_date->diff(new DateTime());
    //echo $since_start->days ? $since_start->days.' dias,' : '';
    echo $since_start->y ? $since_start->y .' anos, ' : '';
    echo $since_start->m ? $since_start->m .' meses, ' : '';
    echo $since_start->d ? $since_start->d .' dias, ' : '';
    echo $since_start->h ? $since_start->h .' horas' : '';
    // echo $since_start->i ? $since_start->i .' minutes,' : '';
    // echo $since_start->s ? $since_start->s .' seconds,' : '';
    echo ' atrás'; ?></div>
        <div class="comment__count"><?php echo child_comment_counter($comment->comment_ID); ?></div>
        <div class="comment__replay"><?php comment_reply_link(array_merge($args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ))); ?></div>
        <div class="comment__edit"><?php edit_comment_link(__('(Edit)'), '  ', ''); ?></div>
    </div>
    <?php if ('div' != $args['style']) : ?>
    </div>
    <?php endif; ?>
    <?php
}


/**
 * Comment Form Placeholder Comment Field
 */
function placeholder_comment_form_field($fields)
{
    $replace_comment = __('Escreva seu comentário', 'yourdomain');

    $fields['comment_field'] = '<p class="comment-form-comment"><label for="comment">' . _x('Comment', 'noun') .
    '</label><textarea id="comment" name="comment" cols="45" rows="8" placeholder="'.$replace_comment.'" aria-required="true"></textarea></p>';

    return $fields;
}
add_filter('comment_form_defaults', 'placeholder_comment_form_field');












/**
 * Custom Avatar Without a Plugin
 */
/*
// 1. Enqueue the needed scripts.
add_action("admin_enqueue_scripts", "ayecode_enqueue");
function ayecode_enqueue($hook)
{
    // Load scripts only on the profile page.
    if ($hook === 'profile.php' || $hook === 'user-edit.php') {
        add_thickbox();
        wp_enqueue_script('media-upload');
        wp_enqueue_media();
    }
}

// 2. Scripts for Media Uploader.
function ayecode_admin_media_scripts()
{
    ?>
    <script>
        jQuery(document).ready(function ($) {
            $(document).on('click', '.avatar-image-upload', function (e) {
                e.preventDefault();
                var $button = $(this);
                var file_frame = wp.media.frames.file_frame = wp.media({
                    title: 'Select or Upload an Custom Avatar',
                    library: {
                        type: 'image' // mime type
                    },
                    button: {
                        text: 'Select Avatar'
                    },
                    multiple: false
                });
                file_frame.on('select', function() {
                    var attachment = file_frame.state().get('selection').first().toJSON();
                    if( "thumbnail" in attachment.sizes ){
                        thumb = attachment.sizes.thumbnail.url;
                    }else{
                        thumb = attachment.sizes.full.url;
                    }
                    $button.siblings('#ayecode-custom-avatar').val( thumb );
                    $button.siblings('.custom-avatar-preview').attr( 'src', thumb );
                });
                file_frame.open();
            });
        });
    </script>
    <?php
}
add_action('admin_print_footer_scripts-profile.php', 'ayecode_admin_media_scripts');
add_action('admin_print_footer_scripts-user-edit.php', 'ayecode_admin_media_scripts');


// 3. Adding the Custom Image section for avatar.
function custom_user_profile_fields($profileuser)
{
    ?>
    <h3><?php _e('Custom Local Avatar', 'ayecode'); ?></h3>
    <table class="form-table ayecode-avatar-upload-options">
        <tr>
            <th>
                <label for="image"><?php _e('Custom Local Avatar', 'ayecode'); ?></label>
            </th>
            <td>
                <?php
                // Check whether we saved the custom avatar, else return the default avatar.
                $custom_avatar = get_the_author_meta('ayecode-custom-avatar', $profileuser->ID);
    if ($custom_avatar == '') {
        $custom_avatar = get_avatar_url($profileuser->ID);
    } else {
        $custom_avatar = esc_url_raw($custom_avatar);
    } ?>
                <img style="width: 96px; height: 96px; display: block; margin-bottom: 15px;" class="custom-avatar-preview" src="<?php echo $custom_avatar; ?>">
                <input type="text" name="ayecode-custom-avatar" id="ayecode-custom-avatar" value="<?php echo esc_attr(esc_url_raw(get_the_author_meta('ayecode-custom-avatar', $profileuser->ID))); ?>" class="regular-text" />
                <input type='button' class="avatar-image-upload button-primary" value="<?php esc_attr_e("Upload Image", "ayecode"); ?>" id="uploadimage"/><br />
                <span class="description">
                    <?php _e('Please upload a custom avatar for your profile, to remove the avatar simple delete the URL and click update.', 'ayecode'); ?>
                </span>
            </td>
        </tr>
    </table>
    <?php
}
add_action('show_user_profile', 'custom_user_profile_fields', 10, 1);
add_action('edit_user_profile', 'custom_user_profile_fields', 10, 1);


// 4. Saving the values.
add_action('personal_options_update', 'ayecode_save_local_avatar_fields');
add_action('edit_user_profile_update', 'ayecode_save_local_avatar_fields');
function ayecode_save_local_avatar_fields($user_id)
{
    if (current_user_can('edit_user', $user_id)) {
        if (isset($_POST[ 'ayecode-custom-avatar' ])) {
            $avatar = esc_url_raw($_POST[ 'ayecode-custom-avatar' ]);
            update_user_meta($user_id, 'ayecode-custom-avatar', $avatar);
        }
    }
}


// 5. Set the uploaded image as default gravatar.
// add_filter('get_avatar_url', 'ayecode_get_avatar_url', 10, 3);
function ayecode_get_avatar_url($url, $id_or_email, $args)
{
    $id = '';
    if (is_numeric($id_or_email)) {
        $id = (int) $id_or_email;
    } elseif (is_object($id_or_email)) {
        if (! empty($id_or_email->user_id)) {
            $id = (int) $id_or_email->user_id;
        }
    } else {
        $user = get_user_by('email', $id_or_email);
        $id = !empty($user) ?  $user->data->ID : '';
    }
    //Preparing for the launch.
    $custom_url = $id ?  get_user_meta($id, 'ayecode-custom-avatar', true) : '';
    var_dump($id);
    // If there is no custom avatar set, return the normal one.
    if ($custom_url == '' || !empty($args['force_default'])) {
        return esc_url_raw('https://wpgd-jzgngzymm1v50s3e3fqotwtenpjxuqsmvkua.netdna-ssl.com/wp-content/uploads/sites/12/2020/07/blm-avatar.png');
    } else {
        return esc_url_raw($custom_url);
    }
}
*/


add_filter('get_avatar_url', 'ayecode_get_avatar_url', 10, 3);
function ayecode_get_avatar_url($url, $id_or_email, $args)
{
    $id = '';
    if (is_numeric($id_or_email)) {
        $id = (int) $id_or_email;
    } elseif (is_object($id_or_email)) {
        if (! empty($id_or_email->user_id)) {
            $id = (int) $id_or_email->user_id;
        }
    } else {
        $user = get_user_by('email', $id_or_email);
        $id = !empty($user) ?  $user->data->ID : '';
    }
    //Preparing for the launch.
    $custom_url = $id ?  wp_get_attachment_image_url(carbon_get_user_meta($id, 'photo_avatar'), 'img_size_4') : '';
    // If there is no custom avatar set, return the normal one.
    if ($custom_url == '' || !empty($args['force_default'])) {
        return esc_url_raw('http://1.gravatar.com/avatar/7905d373cfab2e0fda04b9e7acc8c879?s=96&d=mm&r=g');
    } else {
        return esc_url_raw($custom_url);
    }
}


add_action('print_media_templates', 'wpse_print_media_templates');
function wpse_print_media_templates() { ?>
    <script>
        // Idea via http://unsalkorkmaz.com/wp3-5-media-gallery-edit-modal-change-captions-to-title/
        jQuery( document ).ready( function( $ ){
            jQuery( "script#tmpl-image-details:first" ).remove();
        });
    </script>

    <script type="text/html" id="tmpl-image-details">
        <div class="media-embed">
            <div class="embed-media-settings">
                <div class="column-image">
                    <div class="image">
                        <img src="{{ data.model.url }}" draggable="false" alt="" />

                        <# if ( data.attachment && window.imageEdit ) { #>
                            <div class="actions">
                                <input type="button" class="edit-attachment button" value="<?php esc_attr_e('Edit Original'); ?>" />
                                <input type="button" class="replace-attachment button" value="<?php esc_attr_e('Replace'); ?>" />
                            </div>
                        <# } else if ( ! data.attachment && window.imageEdit ) { #>
                            <!--
                                <div class="actions">
                                    <input type="button" class="replace-attachment button my-theme"  value="<?php esc_attr_e('Replace'); ?>" />
                                </div>
                             -->
                        <# } #>

                    </div>
                </div>
                <div class="column-settings">
                    <?php
                    /** This filter is documented in wp-admin/includes/media.php */
                    if (! apply_filters('disable_captions', '')) : ?>
                        <label class="setting caption">
                            <span><?php _e('Caption'); ?></span>
                            <textarea data-setting="caption">{{ data.model.caption }}</textarea>
                        </label>
                    <?php endif; ?>

                    <label class="setting alt-text">
                        <span><?php _e('Alternative Text'); ?></span>
                        <input type="text" data-setting="alt" value="{{ data.model.alt }}" />
                    </label>

                    <h2><?php _e('Display Settings'); ?></h2>
                    <div class="setting align">
                        <span><?php _e('Align'); ?></span>
                        <div class="button-group button-large" data-setting="align">
                            <button class="button" value="left">
                                <?php esc_html_e('Left'); ?>
                            </button>
                            <button class="button" value="center">
                                <?php esc_html_e('Center'); ?>
                            </button>
                            <button class="button" value="right">
                                <?php esc_html_e('Right'); ?>
                            </button>
                            <button class="button" value="full">
                                <?php esc_html_e('100% Página'); ?>
                            </button>
                            <button class="button active" value="none">
                                <?php esc_html_e('None'); ?>
                            </button>
                        </div>
                    </div>

                    <# if ( data.attachment ) { #>
                        <# if ( 'undefined' !== typeof data.attachment.sizes ) { #>
                            <label class="setting size">
                                <span><?php _e('Size'); ?></span>
                                <select class="size" name="size"
                                    data-setting="size"
                                    <# if ( data.userSettings ) { #>
                                        data-user-setting="imgsize"
                                    <# } #>>
                                    <?php
                                    /** This filter is documented in wp-admin/includes/media.php */
                                    $sizes = apply_filters('image_size_names_choose', array(
                                        'thumbnail' => __('Thumbnail'),
                                        'medium'    => __('Medium'),
                                        'large'     => __('Large'),
                                        'full'      => __('Full Size'),
                                    ));

                                    foreach ($sizes as $value => $name) : ?>
                                        <#
                                        var size = data.sizes['<?php echo esc_js($value); ?>'];
                                        if ( size ) { #>
                                            <option value="<?php echo esc_attr($value); ?>">
                                                <?php echo esc_html($name); ?> &ndash; {{ size.width }} &times; {{ size.height }}
                                            </option>
                                        <# } #>
                                    <?php endforeach; ?>
                                    <option value="<?php echo esc_attr('custom'); ?>">
                                        <?php _e('Custom Size'); ?>
                                    </option>
                                </select>
                            </label>
                        <# } #>
                            <div class="custom-size<# if ( data.model.size !== 'custom' ) { #> hidden<# } #>">
                                <label><span><?php _e('Width'); ?> <small>(px)</small></span> <input data-setting="customWidth" type="number" step="1" value="{{ data.model.customWidth }}" /></label><span class="sep">&times;</span><label><span><?php _e('Height'); ?> <small>(px)</small></span><input data-setting="customHeight" type="number" step="1" value="{{ data.model.customHeight }}" /></label>
                            </div>
                    <# } #>

                    <div class="setting link-to">
                        <span><?php _e('Link To'); ?></span>
                        <select data-setting="link">
                        <# if ( data.attachment ) { #>
                            <option value="file">
                                <?php esc_html_e('Media File'); ?>
                            </option>
                            <option value="post">
                                <?php esc_html_e('Attachment Page'); ?>
                            </option>
                        <# } else { #>
                            <option value="file">
                                <?php esc_html_e('Image URL'); ?>
                            </option>
                        <# } #>
                            <option value="custom">
                                <?php esc_html_e('Custom URL'); ?>
                            </option>
                            <option value="none">
                                <?php esc_html_e('None'); ?>
                            </option>
                        </select>
                        <input type="text" class="link-to-custom" data-setting="linkUrl" />
                    </div>
                    <div class="advanced-section">
                        <h2><button type="button" class="button-link advanced-toggle"><?php _e('Advanced Options'); ?></button></h2>
                        <div class="advanced-settings hidden">
                            <div class="advanced-image">
                                <label class="setting title-text">
                                    <span><?php _e('Image Title Attribute'); ?></span>
                                    <input type="text" data-setting="title" value="{{ data.model.title }}" />
                                </label>
                                <label class="setting extra-classes">
                                    <span><?php _e('Image CSS Class'); ?></span>
                                    <input type="text" data-setting="extraClasses" value="{{ data.model.extraClasses }}" />
                                </label>
                            </div>
                            <div class="advanced-link">
                                <div class="setting link-target">
                                    <label><input type="checkbox" data-setting="linkTargetBlank" value="_blank" <# if ( data.model.linkTargetBlank ) { #>checked="checked"<# } #>><?php _e('Open link in a new tab'); ?></label>
                                </div>
                                <label class="setting link-rel">
                                    <span><?php _e('Link Rel'); ?></span>
                                    <input type="text" data-setting="linkRel" value="{{ data.model.linkClassName }}" />
                                </label>
                                <label class="setting link-class-name">
                                    <span><?php _e('Link CSS Class'); ?></span>
                                    <input type="text" data-setting="linkClassName" value="{{ data.model.linkClassName }}" />
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>
    <?php
}
