@extends('layouts.app')

@section('content')
  <div class="space-x"></div>
  @moduletitle(['title' => get_the_author(),'description' => nl2br(get_the_author_meta('description'))])@endmoduletitle
  @php
    $category = get_queried_object();
    $args = array(
      'author' => get_the_author_ID(),
      's' => sanitize_text_field(get_query_var('s'))
    );
    $query = new WP_Query( $args );
  @endphp
  <div class="main2" data-found_posts="{{ $query->found_posts}}" data-max_num_pages="{{ $query->max_num_pages}}">
    @grid(array( 'items' => $query ))@endgrid
  </div>
  @php wp_reset_postdata() @endphp
@endsection
