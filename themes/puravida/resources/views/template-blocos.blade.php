{{--
  Template Name: Blocos PuraVida
--}}
@extends('layouts.app')
@section('content')
  @while (have_posts()) @php the_post() @endphp
    @php $modules = carbon_get_the_post_meta('modules') @endphp
    @foreach ($modules as $module)
      @if($module['hidden_block'] != true || $module['m_separador'] == true)
        @include('partials.modules.' . $module['_type'], array('module' => $module))
      @endif
    @endforeach
  @endwhile
@endsection
