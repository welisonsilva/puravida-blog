<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>

    <div id="site" role="document">
      @include('partials.header')
      @include('partials.main')
      @include('partials.footer')
    </div>
    @php wp_footer() @endphp
  </body>
</html>
