@extends('layouts.app')

@section('content')
  @php global $wp_query; @endphp
  <div class="space-x"></div>
  @moduletitle(['title' => App::title(),'description' => "Total de <b>" . $wp_query->found_posts . "</b> resultados encontrado para <strong>" . get_query_var('s') . '</strong> '])@endmoduletitle

  @php
    $category = get_queried_object();
    $args = array(
        's' => sanitize_text_field(get_query_var('s'))
      );
  $query = new WP_Query( $args );
  @endphp
  <div class="main2" data-found_posts="{{ $query->found_posts}}" data-max_num_pages="{{ $query->max_num_pages}}">
    @grid(array( 'items' => $query ))@endgrid
  </div>
  @php wp_reset_postdata() @endphp
@endsection
