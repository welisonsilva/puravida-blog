@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @php
      $cat = get_the_category();
      $item = array(
        'id' => get_the_ID(),
        'title' => get_the_title(),
        'image' => array(
          'desktop' => get_the_post_thumbnail_url(get_the_ID()),
          'mobile' => get_the_post_thumbnail_url(get_the_ID()),
        ),
        'date' => get_the_date(),
        'category' => array(
          'name' => $cat[0]->name,
        )
      );
    @endphp

    @articledestaque(['item' => $item])@endarticledestaque

    <section class="l-common-wrapper" style="padding-top: 70px;">
      <div class="l-common-row">
        <div class="sidebar d-flex is-justify-start">
          <div class="toshare fixme">
            <span class="toshare__title">Compartilhar</span>
            <ul class="toshare__items">
              <li class="toshare__item"><a href="whatsapp://send?text=Compartilhando+esse+artigo+'{{ esc_attr($item['title']) }}'+-+{{ esc_url(get_permalink(get_the_ID()))}}" target="_blank" rel="noopener" title="Comparilhar no Whatsapp" aria-label="Comparilhar no Whatsapp">@svg(['icon' => 'whatsapp'])@endsvg</a></li>
              <li class="toshare__item"><a href="https://www.facebook.com/sharer.php?u={{ esc_url(get_permalink(get_the_ID())) }}" target="_blank" rel="noopener" title="Comparilhar no Facebook" aria-label="Comparilhar no Facebook">@svg(['icon' => 'facebook', 'fill' => 1])@endsvg</a></li>
              <li class="toshare__item"><a href="https://twitter.com/intent/tweet?text='{{ urlencode('Compartilhando esse artigo '.esc_attr($item['title'])) }}'&amp;url={{ esc_url(get_permalink(get_the_ID()))}}" target="_blank" rel="noopener" title="Comparilhar no Twitter" aria-label="Comparilhar no Twitter">@svg(['icon' => 'twitter', 'fill' => 1])@endsvg</a></li>
              <li class="toshare__item"><a href="https://www.linkedin.com/shareArticle?url={{ esc_url(get_permalink(get_the_ID()))}}&amp;title={{ urlencode('Compartilhando esse artigo '.esc_attr($item['title'])) }}" target="_blank" rel="noopener" title="Comparilhar no Linkedin" aria-label="Comparilhar no Linkedin">@svg(['icon' => 'linkedin', 'fill' => 1])@endsvg</a></li>
            </ul>
          </div>
        </div>
        <article class="inner-page is-w-75 is-ws-100 is-fln-1 is-fzn-1">
          <div class="post__author is-justify-start is-mb-80">
            <div class="post__author--photo --photoSingle is-mr-50 is-mr-s-10">
              <a href="{{ get_author_posts_url( get_the_author_meta('ID') ) }}" title="{{ get_the_author() }}">
                {!! get_avatar( get_the_author_meta( 'ID' ), 100 ) !!}
              </a>
            </div>
            <div class="post__author--name">
                  <span class="name is-fz-18 is-mb-10 d-flex">
                    <a href="{{ get_author_posts_url( get_the_author_meta('ID') ) }}" title="{{ get_the_author() }}">
                      {{ get_the_author() }}
                    </a>
                  </span>
                  <div class="desc name">{!! nl2br(get_the_author_meta('description')) !!}</div>
            </div>
        </div>

          @php the_content(); @endphp
        </article>
      </div>
    </section>

    <section class="l-common-wrapper comentarios" style="padding-top: 70px;">
      <div class="l-common-row">
        <div class="sidebar d-flex is-justify-start"></div>
        <div class="is-w-75 is-ws-100">
          {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
          @php comments_template('/partials/comments.blade.php') @endphp
        </div>
      </div>
    </section>


  @endwhile
@endsection
