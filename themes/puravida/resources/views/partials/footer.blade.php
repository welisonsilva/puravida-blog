<footer id="footer">
  <div class="l-common-wrapper">
    <div class="l-common-row is-justify-between is-align-stretch flex-s-column">
      <div class="col d-flex flex-column is-justify-around is-align-start is-w-25 is-ws-100">
        @if (is_active_sidebar('sidebar-footer'))
          @php dynamic_sidebar('sidebar-footer'); @endphp
        @endif
        {{-- @menu(['location' => 'navigation_social', 'container_class' => 'box-social  is-mt-s-30', 'class' => 'social d-flex is-justify-between'])@endmenu --}}
      </div>
      <div class="menu is-mx-65 is-mx-s-0 is-w-50 is-ws-100 is-mt-s-30">@menu(['location' => 'secondary_navigation', 'container_class' => 'box-editorial', 'class' => 'editorial d-flex flex-wrap'])@endmenu</div>
      <div class="is-w-25  is-ws-100 is-mt-s-30">@menu(['location' => 'third_navigation', 'container_class' => 'box-ajuda', 'class' => 'ajuda'])@endmenu</div>
    </div>
    {{--  --}}
    <div class="l-common-row is-mt-40">
      <div class="is-w-100"><hr /></div>
    </div>
    {{--  --}}
    <div class="l-common-row is-justify-between is-align-center is-pt-20 flex-s-column">
      <div class="col">
        <a href="{{ home_url('/') }}"><img src="@asset('images/logo-rodape.png')" alt="{{ get_bloginfo('name', 'display') }}" /></a>
      </div>
      <div class="coopy">
        <span class="is-fz-10">CNPJ: 12.966.706/0001-91 | NEO VIDA COMERCIO E IMPORTAÇÃO DE PRODUTOS NATURAIS S/A</span>
      </div>
    </div>
  </div>
</footer>
