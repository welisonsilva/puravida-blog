@if ( isset($module))
  @php
    $slide = array();

    $args = array(
      'post_type'  => array('post'),
      'posts_per_page' => trim($module['limit']) ?? 12,
      'post_status'    => 'publish',
    );

    if( $module['origin_posts'] == 1){
      $args['post__in'] = array_column($module['posts'], 'id');
    }

    if( $module['categories'] && $module['origin_posts'] == 2){
      $args['cat'] = $module['categories'];
    }

    $items = new WP_Query( $args);

    if( $module['categories'] && $module['origin_posts'] == 4){

      $slide['box_title'] = array(
        'title' => get_cat_name($module['categories']),
        'description' => category_description($module['categories']),
        'category' => array(
          'id' => $module['categories'],
          'title' => 'Veja tudo sobre '.get_cat_name($module['categories']),
          'link' => get_category_link($module['categories'])
        )
      );
    }

    if( $module['origin_posts'] == 2 || $module['origin_posts'] == 1){
      $slide['box_title'] = array(
        'title' => $module['title'],
        'description' => $module['description'],
      );

      if( $module['enable_button'] ){
        $slide['box_title']['category'] = array(
          'title' => $module['button_text'],
          'link' => $module['button_link']
        );
      }
    }


    $slide['items'] = $items;
    wp_reset_postdata();
  @endphp
  @slide($slide)@endslide
@endif
