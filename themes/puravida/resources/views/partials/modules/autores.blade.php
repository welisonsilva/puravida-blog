@php
  $items = array();
  $args = array(
    'orderby' =>'display_name',
  );
  if( $module['user'] && $module['origin_users'] == 1){
    $args['include'] = array_column($module['user'], 'id');
  }
  $wp_user_query = new WP_User_Query($args);
  $authors = $wp_user_query->get_results();
  $items['items'] = App::itemsPrepareUser($authors);

  if( trim($module['title']) ){
    $items['boxtitle'] = $module['title'];
  }
@endphp
@authors($items)@endauthors

