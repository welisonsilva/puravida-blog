@if ( isset($module))
  @php
    $args = array(
      //'cat' => $module['categories'],
      'posts_per_page' => 1,
    );

    if( $module['origin_posts'] == 1){
      $args['post__in'] = array($module['posts']);
    }

    if( $module['categories'] && $module['origin_posts'] == 4){
      $args['cat'] = $module['categories'];
    }

    $query = new WP_Query( $args );
    $item =  $query->posts[0];

  @endphp
  @php wp_reset_postdata() @endphp

  <section>
    <div class="d-flex post--destaque is-my-100 is-my-s-30">
      @article(App::itemsPrepare($item))@endarticle
    </div>
  </section>
@endif




