@if ( isset($module))
  @php
    $args = array(
          'cat' => $module['categories'],
          'posts_per_page' => trim($module['limit']) ?? 4,
        );
    $query = new WP_Query( $args );

    $grid = array();

    if( !$module['disable_title'] ){
      $grid['box_title'] = array(
        'title' => get_cat_name($module['categories']),
        'description' => category_description($module['categories']),
        'category' => array(
          'id' => 0,
          'title' => 'Veja tudo sobre '.get_cat_name($module['categories']),
          'link' => get_category_link($module['categories'])
        )
      );
    }

    $grid['items'] = $query;

  @endphp
  @php wp_reset_postdata() @endphp
  @grid($grid)@endgrid
@endif

