<form role="search" method="get" class="search-form is-px-26 is-py-13" action="{{ esc_url( home_url( '/' ) ) }}">
  <label class="d-flex is-justify-between is-align-center">
    <span class="screen-reader-text">{{ _x( 'Search for:', 'label' ) }}</span>
    @svg(['icon' => 'search', 'fill' => 1])@endsvg
    <input type="search" class="search-field" placeholder="{!! esc_attr_x( 'Search &hellip;', 'placeholder' ) !!}" value="{{ get_search_query() }}" name="s" />
  </label>
  <input type="submit" class="search-submit" value="{{ esc_attr_x( 'Search', 'submit button' ) }}" />
</form>
