<article class="post item" id="post--{{ esc_attr($id) }}">
  @if ($link)<a href="{{ esc_url($link) }}" title="{{ esc_attr($title) }}">@endif
    <figure class="post__image" style="background-image: url({{ esc_url($image['desktop']) }});">
      <img src="{{ esc_url($image['desktop']) }}" class="img-responsive desktop" data-slidecropimg="{{ wp_get_attachment_image_url(get_post_thumbnail_id($id), 'img_size_3') }}" />
      {{-- <img src="{{ esc_url($image['mobile']) }}" class="img-responsive mobile" /> --}}
    </figure>
  @if ($link)</a>@endif
  <div class="post__parent">
    @if ($category)
      <div class="post__category">
        <span>{{ $category['name'] }}</span>
      </div>
    @endif
    <h2 class="post__title">
      @if ($link)<a href="{{ esc_url($link) }}" title="{{ esc_attr($title) }}">@endif
        {{ esc_html($title) }}
      @if ($link)</a>@endif
    </h2>

    @if ($date)
      <div class="post__date">{{ $date }}</div>
    @endif

    @if ($author)
      <div class="post__author">
          <div class="post__author--photo">
            <a href="{{ esc_url($author['link']) }}" title="{{ esc_attr($author['name']) }}">
              <img src="{{ esc_url($author['photo']) }}" alt="" />
            </a>
          </div>
          <div class="post__author--name">
              <a href="{{ esc_url($author['link']) }}" title="{{ esc_attr($author['name']) }}">
                <span class="is-fz-12">{{ esc_html($author['name']) }}</span>
              </a>
          </div>
      </div>
    @endif
  </div>
</article>
