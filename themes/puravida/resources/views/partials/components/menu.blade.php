@if ($location && has_nav_menu($location))
  @php
    $menu_item = wp_get_nav_menu_object( get_nav_menu_locations( $location )[ $location ] )->name;
    $svg = \App\template('partials.components.tags.svg', ['icon' => 'arrow-up-right', 'fill' => 1]);
  @endphp
  {!! wp_nav_menu(['theme_location' => $location, 'container_class' => ( $container_class ?? ''), 'menu_class' => 'nav '.( $class ?? ''), 'items_wrap' => '<h3 class="title">'. $svg .esc_html($menu_item).'</h3><ul id="%1$s" class="%2$s">%3$s</ul>']) !!}
@endif
