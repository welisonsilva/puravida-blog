@if ($icon)
  <svg class="{{ !$fill ? 'is-no-stroke' : 'is-no-fill'}} {{$icon}}">
    <use xlink:href="{{App::getPatchImageCommonSVG()}}#{{ $icon }}" />
  </svg>
@endif
