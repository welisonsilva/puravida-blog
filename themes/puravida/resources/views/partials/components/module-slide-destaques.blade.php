@php
  $args = array(
    'post_type'  => array('post'),
    'posts_per_page' => 10,
    'post_status'    => 'publish',
  );

  if( isset($module) && $module['origin_posts'] == 1){
    $args['post__in'] = array_column($module['posts'], 'id');
  }

  $items = new WP_Query( $args);
  wp_reset_postdata();
@endphp

<div>
  <section class="l-common-wrapper slick-wrapper-ajust">
    <div >
      @if ( isset($module) && trim($module['title']))
        <h2 class="title-blog">{{ esc_html($module['title']) }}</h2>
      @endif
    </div>
    <div class="slick loading post--one post--no-cat-date" >
      @foreach ($items->posts as $item)
        @article(App::itemsPrepare($item))@endarticle
      @endforeach
    </div>
  </section>
</div>
