@if ( isset($box_title) )
  @moduletitle($box_title)@endmoduletitle
@endif
{{-- Grid --}}
{{-- @dump($items->found_posts) --}}
{{-- @dump($items->max_num_pages) --}}
<div class="wrapper">
  <div class="module__content masonry post--one post--no-cat-date container masonry--{{ (count($items->posts) > 4) ? '3' : '2'}} masonry--total-{{$items->found_posts}}">
    @foreach ($items->posts as $item)
      @if ($loop->iteration % 7 == 0 && !is_front_page())
          @include('partials.components.tags.grid_quebra_top')
          <section>
            <div class="d-flex post--destaque is-my-100 is-my-s-30">
              @article(App::itemsPrepare($item))@endarticle
            </div>
          </section>
          @include('partials.components.tags.grid_quebra_bottom')
        @else
          @article(App::itemsPrepare($item))@endarticle
      @endif
    @endforeach
    @if ( isset($box_title) && isset($box_title['category']) )
        <div class="item is-mr-30 is-mb-35 is-pl-65 is-pl-s-0 is-pt-20 is-ws-100"><p>@linkmore($box_title['category'])@endlinkmore</p></div>
      @endif
  </div>
</div>
{{-- Grid --}}
