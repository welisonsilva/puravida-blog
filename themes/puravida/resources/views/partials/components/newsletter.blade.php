<section class="l-common-wrapper m_newsletter  is-mb-80 is-mt-60">
  <div class="l-common-row flex-column is-justify is-align-center">
    <div class="is-w-75 is-ws-100 m_newsletter__block">
      {!! do_shortcode(get_theme_mod( "form_newsletter")) !!}
    </div>
  </div>
</section>
