<section class="s_module">

  @if ($title)
    <div class="l-common">
      <div class="l-common-row is-justify-evenly is-align-center s_module__title">
        <h2 data-titulo="{!! $title !!}" ><span data-titulo="{!! $title !!}">{!! $title !!}</span></h2>
      </div>
    </div>
  @endif

  @if ($description)
    <div class="l-common-row is-justify-evenly is-align-center is-py-50 is-py-s-30 s_module__description">
      <div class="s_module__description--inner">
        {!! $description !!}
      </div>
    </div>
  @endif

</section>
