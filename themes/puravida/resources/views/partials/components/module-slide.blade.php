@if ( isset($box_title) )
  @moduletitle($box_title)@endmoduletitle
@endif
<section>
    <div class="m-slide d-flex is-w-100 box-scroll">
      <div class="m-slide--scroll is-pb-150 post--one post--no-cat-date">
        @if ($items)
          @foreach ($items->posts as $item)
            @article(App::itemsPrepare($item))@endarticle
          @endforeach
        @endif
      </div>
    </div>
</section>

@if ( isset($box_title) && isset($box_title['category']) )
  <section class="l-common-wrapper ajusteBoxScrollBottom">
    <div class="l-common-row is-justify-center is-align-center" style="margin-top: -80px;    z-index: 1;">
      @linkmore($box_title['category'])@endlinkmore
    </div>
  </section>
@endif
