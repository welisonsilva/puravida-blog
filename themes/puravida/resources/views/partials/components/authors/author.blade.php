@if ( $image )

<article class="author item" id="author--{{ esc_attr($id) }}">
    <a href="{{ esc_url($link) }}" title="{{ esc_attr($name) }}">
  <figure class="author__image">

    <img src="{{ esc_url($image) }}" class="img-responsive" />
  </figure>
    </a>
  <div class="author__info">
    <div class="author__name">
      <span>{{ esc_html($name) }}</span>
    </div>
    <div class="author__cargo">
      <span>{{ esc_html($cargo) }}</span>
    </div>
    <div class="author__button">
      <a href="{{ esc_url($link) }}" title="{{ esc_attr($name) }}">
        <span >+ Leia mais</span>
      </a>
    </div>
  </div>
</article>
@endif
