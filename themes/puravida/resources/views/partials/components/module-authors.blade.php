@if ( isset($boxtitle))
  <section class="l-common-wrapper author-title is-mt-150">
    <div class="l-common-row is-justify-start is-align-center">
      <h2 class="title">{{ $boxtitle }}</h2>
    </div>
  </section>
@endif

<section>
    <div class="m-slide d-flex is-w-100 box-scroll-author" style="margin-top: 80px;">
      <div class="m-slide--scroll authors">
        @if ($items)
          @foreach ($items as $item)
            @author($item)@endauthor
          @endforeach
        @endif
      </div>
    </div>
</section>

