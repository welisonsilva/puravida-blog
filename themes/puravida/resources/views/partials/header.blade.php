<header class="d-flex is-ps-fixed is-w-100">
  <div id="progress-bar"></div>
  <div class="l-common-wrapper">
    <div class="l-common-row is-justify-between is-align-center is-py-10 is-py-s-0">
      <div class="logo">
        <a class="brand" href="{{ home_url('/') }}"><img src="@asset('images/logo-puravida.png')" alt="{{ get_bloginfo('name', 'display') }}" /></a>
      </div>

      @if (is_single())
        <div class="post-title-single">{!! App::title() !!}</div>
        <div class="post-controllers-single d-flex is-justify-between is-align-center">
          <div class="tx-espacamento d-flex is-justify-between is-align-center">
            <span class="title">Espaçamento do texto</span>
            <span class="flx fl-1 active" title="Espaçamento do texto 1">1</span>
            <span class="flx fl-2" title="Espaçamento do texto 2">2</span>
          </div>
          <div class="tx-tamanho d-flex is-justify-between is-align-center">
            <span class="title">Tamanho do texto</span>
            <span class="fzx fl-1 active" title="Tamanho do texto 1">A</span>
            <span class="fzx fl-2" title="Tamanho do texto 1">A</span>
          </div>
        </div>
      @else
        <nav class="menu flex-grow-1">
          @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav d-flex is-justify-around']) !!}
          @endif
          <span class="nav-open"><span></span></span>
        </nav>
        <div class="search is-mx-20 is-mx-s-5">
          {!! get_search_form() !!}
        </div>
        @if (!get_theme_mod('puravida_enable_button'))
          <div class="back-home">
            <a href="{{get_theme_mod( "puravida_button" )}}" title="{{get_theme_mod( "puravida_text" )}}" class="button b--verde b--header">@svg(['icon' => 'arrow-left', 'fill' => 1])@endsvg {{get_theme_mod( "puravida_text" )}}</a>
          </div>
        @endif
      @endif
    </div>
  </div>
</header>
