@php
if (post_password_required()) {
  return;
}
@endphp

<section id="comments" class="comments">

  @if (have_comments())
  <div class="comments__header d-flex is-justify-between is-align-center">
    <div class="comments__title">
      <span>Comentários</span>
    </div>
    <div class="comments__total">
      <span>{{ get_comments_number() }} comentário(s)</span>
    </div>
  </div>
  <div class="comments__list">
    <ol class="comment-list">
      {!! wp_list_comments(['style' => 'ol', 'short_ping' => true, 'callback' => 'fnComments', 'avatar_size' => 27, 'reply_text' => __('<svg class="is-no-fill replay"><use xlink:href="'.App::getPatchImageCommonSVG().'#comment-replay"></use></svg>Responder', 'textdomain')]) !!}
    </ol>
  </div>
  <div class="comments__pagination">
    @if (get_comment_pages_count() > 1 && get_option('page_comments'))
      <nav>
        <ul class="pager">
          @if (get_previous_comments_link())
            <li class="previous">@php previous_comments_link(__('&larr; Older comments', 'sage')) @endphp</li>
          @endif
          @if (get_next_comments_link())
            <li class="next">@php next_comments_link(__('Newer comments &rarr;', 'sage')) @endphp</li>
          @endif
        </ul>
      </nav>
    @endif
  </div>

  @endif
  <div class="comments__closed">
  @if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments'))
    <div class="alert alert-warning">
      {{ __('Comments are closed.', 'sage') }}
    </div>
  @endif
  </div>
  <div class="comments__form">
    @php
      $commenter = wp_get_current_commenter();
      $req = get_option( 'require_name_email' );
      $aria_req = ( $req ? " aria-required='true'" : '' );
      $comments_args = array(
          'fields' =>  array(
          'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
              '<input id="author" name="author" type="text" placeholder="Nome" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
          'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email' ) . '</label> ' . ( $req ? '<span class="required">*</span>' : '' ) .
              '<input id="email" name="email" type="text" placeholder="E-maiil" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',
      ),
          'submit_button' => '<button name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s"><svg class="is-no-fill replay"><use xlink:href="' . App::getPatchImageCommonSVG() . '#comment-replay"></use></svg> Comentar</button>'
      );
      comment_form($comments_args);

    @endphp
  </div>
</section>
