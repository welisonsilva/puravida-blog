<main class="main is-pt-70  is-pt-s-20" role="main">
  @yield('content')
  @include('partials.components.newsletter')
</main>
