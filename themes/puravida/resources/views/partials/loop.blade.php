<section class="l-common-wrapper">
    @while(have_posts()) @php the_post() @endphp
      <h1>{{ the_title()}} {{ the_category()}}</h1>
    @endwhile
</section>
