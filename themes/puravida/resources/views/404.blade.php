@extends('layouts.app')

@section('content')
  <div class="space-x"></div>
  @moduletitle(['title' => App::title()])@endmoduletitle
    <section class="l-common-wrapper error-404" style="padding-top: 70px;">
      <div class="l-common-row">
        <div >
          @if (!have_posts())
          <div class="alert alert-warning is-fz-16 is-fw-100 text-alingn-center">
            {{ __('Desculpe, mas a página que você está tentando visualizar não existe.', 'sage') }}
          </div>
          {{-- {!! get_search_form(false) !!} --}}
        @endif
        </div>
      </div>
    </section>
@endsection
