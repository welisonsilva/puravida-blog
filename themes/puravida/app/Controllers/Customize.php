<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Customize_Control;

class Customize extends Controller
{
    public function __construct()
    {
        add_action('customize_register', array($this, 'your_theme_new_customizer_settings'));
    }

    public function your_theme_new_customizer_settings($wp_customize)
    {
        $wp_customize->add_setting("puravida_text", array(
            "default" => "Voltar para o Prime",
            "transport" => "postMessage",
        ));

        $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize,
            "puravida_text",
            array(
                "label" => __("Texto do botão header"),
                "section" => "title_tagline",
                "settings" => "puravida_text",
                "type" => "text",
            )
        ));

        $wp_customize->add_setting("puravida_button", array(
            "default" => "#",
            "transport" => "postMessage",
        ));
        $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize,
            "puravida_button",
            array(
                "label" => __("Link do botão header"),
                "section" => "title_tagline",
                "settings" => "puravida_button",
                "type" => "text",
            )
        ));

        $wp_customize->add_setting('puravida_enable_button', array(
            'default'    => '1'
        ));

        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                'puravida_enable_button',
                array(
                    'label'     => "Ocultar botão header?",
                    'section'   => 'title_tagline',
                    'settings'  => 'puravida_enable_button',
                    'type'      => 'checkbox',
                )
            )
        );

        /**
         * ActiveCampaign
         */
        $wp_customize->add_section("ads", array(
            "title" => __("ActiveCampaign"),
            "priority" => 30,
        ));
        $wp_customize->add_setting("form_newsletter", array(
            "default" => "",
            "transport" => "postMessage",
        ));
        $wp_customize->add_control(new WP_Customize_Control(
            $wp_customize,
            "form_newsletter",
            array(
                "label" => __("ActiveCampaign"),
                "section" => "ads",
                "settings" => "form_newsletter",
                "type" => "textarea",
            )
        ));
    }
}
