<?php
namespace App\Controllers;

use WP_Query;
use Sober\Controller\Controller;

class Ajax extends Controller
{
    public function __construct()
    {
        add_action('wp_ajax_get_modal', array($this, 'Ajax::getloadingMorePosts'));
        add_action('wp_ajax_nopriv_get_modal', array($this, 'Ajax::getloadingMorePosts'));
    }

    /**
     *
     */
    public function getloadingMorePosts()
    {
        if (! check_ajax_referer('puravida-security-nonce', 'security', false)) {
            wp_send_json_error('Invalid security token sent.');
            wp_die();
        }

        $query = new WP_Query(array(
            'cat'=> (int) sanitize_text_field($_GET['category']) ?? [],
            'author'=> (int) sanitize_text_field($_GET['author']) ?? [],
            'paged' => (int) sanitize_text_field($_GET['paged']),
        ));
        wp_reset_postdata();
        echo \App\template('partials.components.module-grid', array( 'items' => $query));
        wp_die();
    }
}
