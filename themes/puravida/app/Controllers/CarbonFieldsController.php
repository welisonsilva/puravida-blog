<?php

namespace App\Controllers;

use Carbon_Fields\Block;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class CarbonFieldsController
{
    public function __construct()
    {
        add_action('carbon_fields_register_fields', [$this,'fieldsBlockModules']);
        add_action('carbon_fields_register_fields', [$this,'fieldsUser']);
        // add_action('carbon_fields_register_fields', [$this,'field_text_block']);
    }

    /**
     * fieldsBlockModules
     *
     * @return void
     */
    public function fieldsBlockModules()
    {
        Container::make('post_meta', 'Blocos')
        ->where('post_type', '=', 'page')
        ->add_fields(array(
            Field::make('complex', 'modules', __('Slider'))
                ->setup_labels(array(
                    'plural_name' => 'Adicionar',
                    'singular_name' => 'adicionar',
                ))

                // Slider destaque (Blog)
                ->add_fields('slide_destaques', __('Slide Destaques'), array(
                    Field::make('checkbox', 'hidden_block', 'Ocultar bloco?')
                        ->set_help_text('Marque essa opção, para ocultar esse bloco no site.')
                        ->set_option_value('yes'),
                    Field::make('text', 'title', __('Titulo')),
                    // Field::make('checkbox', 'enable_title', __('Exibir titulo')),
                    Field::make('select', 'origin_posts', __('Origem dos posts?'))
                        ->add_options(array(
                            '1' => __('Selecionar posts'),
                            '2' => __('Ultimos posts cadastrados'),
                            // '3' => __('Apenas posts marcados como destaque na home.'),
                        )),
                    Field::make('association', 'posts')
                        ->set_required(true)
                        ->set_min(3)
                        ->set_types(array(
                            array(
                                'type' => 'post',
                                'post_type' => 'post',
                            )
                        ))
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_posts',
                                'value' => 1,
                            )
                        ))
                ))

                // Grid posts categoria
                ->add_fields('grid_categories', __('Grid Categoria'), array(
                    Field::make('checkbox', 'hidden_block', 'Ocultar bloco?')
                        ->set_help_text('Marque essa opção, para ocultar esse bloco no site.')
                        ->set_option_value('yes'),
                    // Field::make('text', 'title', __('Slide Title'))
                    //     ->set_help_text('....'),
                    Field::make('checkbox', 'disable_title', __('Ocultar titulo'))
                        ->set_option_value('yes'),
                    Field::make('select', 'categories', __('Categoria'))
                        ->add_options(array($this, 'getCategories')),
                    Field::make('text', 'limit', __('Total a ser exibido'))
                        ->set_default_value(4),
                ))


                // Slider Vertical
                ->add_fields('slide_vertical', __('Slide Vertical'), array(
                    Field::make('checkbox', 'hidden_block', 'Ocultar bloco?')
                        ->set_help_text('Marque essa opção, para ocultar esse bloco no site.')
                        ->set_option_value('yes'),
                    Field::make('text', 'title', __('Titulo'))
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_posts',
                                'value' => 4,
                                'compare' => "!="
                            )
                        )),
                    Field::make('text', 'description', __('Descrição'))
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_posts',
                                'value' => 4,
                                'compare' => "!="
                            )
                        )),
                    Field::make('checkbox', 'enable_button', __('Exibir botão?')),
                    Field::make('text', 'button_text', __('Texto do link'))
                        ->set_width(50)
                        ->set_conditional_logic(array(
                            'relation' => 'AND',
                            array(
                                'field' => 'origin_posts',
                                'value' => 4,
                                'compare' => "!="
                            ),
                            array(
                                'field' => 'enable_button',
                                'value' => 1,
                            )
                        )),
                    Field::make('text', 'button_link', __('Link'))
                        ->set_width(50)
                        ->set_conditional_logic(array(
                            'relation' => 'AND',
                            array(
                                'field' => 'origin_posts',
                                'value' => 4,
                                'compare' => "!="
                            ),
                            array(
                                'field' => 'enable_button',
                                'value' => 1,
                            )
                        )),
                    Field::make('select', 'origin_posts', __('Origem dos posts?'))
                        ->add_options(array(
                            '1' => __('Selecionar posts'),
                            '2' => __('Ultimos posts cadastrados'),
                            // '3' => __('Apenas posts marcados como destaque na home.'),
                            '4' => __('Posts de uma categoria'),
                        )),
                    Field::make('association', 'posts')
                        ->set_min(3)
                        ->set_types(array(
                            array(
                                'type' => 'post',
                                'post_type' => 'post',
                            )
                        ))
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_posts',
                                'value' => 1,
                            )
                        )),
                    Field::make('select', 'categories', __('Categoria'))
                        ->add_options(array($this, 'getCategories'))
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_posts',
                                'value' => 4,
                            )
                        )),
                    Field::make('text', 'limit', __('Total a ser exibido'))
                        ->set_default_value(12)
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_posts',
                                'value' => 1,
                                'compare' => "!="
                            ),
                        )),
                ))

                // Slider Vertical
                ->add_fields('post_destaque', __('Post destaque'), array(
                    Field::make('checkbox', 'hidden_block', 'Ocultar bloco?')
                        ->set_help_text('Marque essa opção, para ocultar esse bloco no site.')
                        ->set_option_value('yes'),
                    Field::make('select', 'origin_posts', __('Origem dos posts?'))
                        ->add_options(array(
                            '1' => __('Selecionar um post'),
                            '2' => __('Ultimo post cadastrados'),
                            // '3' => __('Apenas posts marcados como destaque na home.'),
                            '4' => __('Ultimo post de uma categoria'),
                        )),
                    Field::make('select', 'posts', __('Post'))
                        ->add_options(array($this, 'getPosts'))
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_posts',
                                'value' => 1,
                            )
                        )),
                    Field::make('select', 'categories', __('Categoria'))
                        ->add_options(array($this, 'getCategories'))
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_posts',
                                'value' => 4,
                            )
                        )),
                ))

                // Slider Vertical
                ->add_fields('autores', __('Autores'), array(
                    Field::make('checkbox', 'hidden_block', 'Ocultar bloco?')
                        ->set_help_text('Marque essa opção, para ocultar esse bloco no site.')
                        ->set_option_value('yes'),
                    Field::make('text', 'title', __('Titulo')),
                    Field::make('select', 'origin_users', __('Origem autores?'))
                        ->add_options(array(
                            '1' => __('Selecionar autores'),
                            '2' => __('Lista de autores cadastrados')
                        )),
                    Field::make('association', 'user')
                        ->set_types(array(
                            array(
                                'type' => 'user',
                            )
                        ))
                        ->set_conditional_logic(array(
                            array(
                                'field' => 'origin_users',
                                'value' => 1,
                            )
                        )),

                    ))

                //Separador
                ->add_fields('separador', __('Separador'), array(
                        Field::make('checkbox', 'm_separador', __('Separador'))
                        ->set_option_value('yes')
                    )),


        ));
    }

    /**
     * getCategories
     *
     * @return void
     */
    public function getCategories()
    {
        $return = array();
        $categories = get_categories(array(
            'orderby' => 'name',
            'order'   => 'ASC'
        ));
        foreach ($categories as $category) {
            $return[$category->term_id] = sprintf(esc_html__('%s (%s)', 'textdomain'), $category->name, $category->count);
        }
        return $return;
    }

    /**
     * getCategories
     *
     * @return void
     */
    public function getPosts()
    {
        $return = array();
        $posts = get_posts(array(
            'post_type'  => array('post'),
            'posts_per_page' => -1,
            'post_status'    => 'publish',
            'orderby' => 'date',
            'order'   => 'ASC'
        ));
        foreach ($posts as $post) {
            $return[$post->ID] = sprintf(esc_html__('%s (%s)', 'textdomain'), $post->post_title, $post->post_date);
        }
        return $return;
    }

    /**
     * fieldsUser
     *
     * @return void
     */
    public function fieldsUser()
    {
        Container::make('user_meta', __('Campos adicionais'))
            ->add_fields(array(
                Field::make('text', 'cargo', 'Cargo'),
                Field::make('image', 'photo_destaque', __('Photo de destaque')),
                Field::make('image', 'photo_avatar', __('Image'))
            ));
    }

    /**
     * Editor Guttemberg
     *
     * @return void
     */
    public function field_text_block()
    {
        Block::make(__('Grid Categoria'))
            ->add_fields(array(
                Field::make('text', 'heading', __('Block Heading')),
                Field::make('image', 'image', __('Block Image')),
                Field::make('rich_text', 'content', __('Block Content')),
                Field::make('association', 'crb_association')
                    ->set_types(array(
                        array(
                            'type' => 'term',
                            'taxonomy' => 'category',
                        )
                    ))
            ))
            ->set_render_callback(function ($fields, $attributes, $inner_blocks) {
                ?>

                <div class="block">
                    <div class="block__heading">
                        <h1><?php echo esc_html($fields['heading']); ?></h1>
                    </div><!-- /.block__heading -->

                    <div class="block__image">
                        <?php echo wp_get_attachment_image($fields['image'], 'full'); ?>
                    </div><!-- /.block__image -->

                    <div class="block__content">
                        <?php echo apply_filters('the_content', $fields['content']); ?>
                    </div><!-- /.block__content -->
                </div><!-- /.block -->

                <?php
            });
    }
}
