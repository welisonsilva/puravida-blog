<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return wp_strip_all_tags(substr(get_the_archive_title(), strpos(get_the_archive_title(), ': ') + 2));
        }
        if (is_search()) {
            return sprintf(__('Resultado da busca', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }


        return get_the_title();
    }

    public static function isHomePage()
    {
        return is_front_page();
    }
    /**
     * getPatch
     *
     * @return void
     */
    public static function getPatchImageCommonSVG()
    {
        $data = file_get_contents(get_theme_file_path().'/dist/assets.json');
        $json = json_decode($data, true);
        return get_theme_file_uri()."/dist/".$json['images/common.svg'];
    }

    public static function itemsPrepare($item): array
    {
        $cat = get_the_category($item->ID);
        return array(
            'id' => $item->ID,
            'title' => $item->post_title,
            'image' => array(
                'desktop' => get_the_post_thumbnail_url($item->ID, 'img_size_1'),
                'mobile' => get_the_post_thumbnail_url($item->ID, 'img_size_6'),
            ),
            'link' => get_permalink($item->ID),
            'date' => get_the_date(),
            'category' => array(
                'id' => $cat[0]->term_id,
                'name' => $cat[0]->name,
                'link' => get_category_link($cat[0]->term_id)
            ),
            'author' => array(
                'id' => $item->post_author,
                'name' => get_the_author_meta('display_name', $item->post_author),
                'photo' => get_avatar_url($item->post_author, ['size' => '30']) ,
                'link' => get_author_posts_url($item->post_author),
            )
        );
    }

    public static function itemsPrepareUser($items): array
    {
        return array_map(function ($item) {
            return array(
                'id' => $item->ID,
                'name' => get_author_name($item->ID),
                'cargo' => carbon_get_user_meta($item->ID, 'cargo'),
                'image' => wp_get_attachment_image_url(carbon_get_user_meta($item->ID, 'photo_destaque'), 'img_size_2'),
                'link' => get_author_posts_url($item->ID),
            );
        }, $items);
    }
}
