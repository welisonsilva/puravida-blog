<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

use App\Controllers\CarbonFieldsController;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', false, null);
    wp_enqueue_style('slick-theme', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css', false, null);

    wp_enqueue_script('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', ['jquery'], null, true);
    wp_enqueue_script('masonry', '//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', ['jquery'], null, true);

    if (!is_single()) {
        wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
        wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    }

    if (is_single()) {
        wp_enqueue_style('sage/single.css', asset_path('styles/single.css'), false, null);
        wp_enqueue_script('sage/single.js', asset_path('scripts/single.js'), ['jquery'], null, true);
    }

    wp_localize_script(
        'sage/main.js',
        'puravida_ajax_object',
        array(
            'ajax_url'  => admin_url('admin-ajax.php'),
            'security'  => wp_create_nonce('puravida-security-nonce'),
            'category'  => get_query_var('cat'),
            'author'  => get_the_author_ID()
        )
    );

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}, 100);


/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    add_image_size('img_size_1', 675, 500, array( 'top', 'left'));
    add_image_size('img_size_2', 285, 420, true); // imagem user
    add_image_size('img_size_3', 145, 410, true); // Imagem de corte para slider
    add_image_size('img_size_4', 100, 100, true); // gravatar user
    add_image_size('img_size_5', 1440, 700, true);
    add_image_size('img_size_6', 400, 500, true); // imagem mobile

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'secondary_navigation' => __('Secondary Navigation', 'sage'),
        'third_navigation' => __('Third Navigation', 'sage'),
        'navigation_social' => __('Redes Sociais', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
    sage('blade')->compiler()->component('partials.components.tags.svg', 'svg');
    sage('blade')->compiler()->component('partials.components.tags.divisor', 'divisor');
    sage('blade')->compiler()->component('partials.components.menu', 'menu');
    sage('blade')->compiler()->component('partials.components.module-grid', 'grid');
    sage('blade')->compiler()->component('partials.components.module-slide', 'slide');
    sage('blade')->compiler()->component('partials.components.module-slide-destaques', 'slidedestaques');
    sage('blade')->compiler()->component('partials.components.module-title', 'moduletitle');
    sage('blade')->compiler()->component('partials.components.authors.author', 'author');
    sage('blade')->compiler()->component('partials.components.module-authors', 'authors');
    sage('blade')->compiler()->component('partials.components.article.article', 'article');
    sage('blade')->compiler()->component('partials.components.article.destaque', 'articledestaque');
    sage('blade')->compiler()->component('partials.components.link-more-category', 'linkmore');
});


/**
 * Adiciona template formulario de busca
 */
add_filter('get_search_form', function () {
    $form = '';
    echo template('partials.searchform');
    return $form;
});


$carbonFieldsController = new CarbonFieldsController();
